package com.indra.apijava.utils.file.rules.csv;

import java.lang.reflect.Field;
import java.util.HashMap;

/***
 * @author Joao
 * This interface is used to specify a set of rules (like delimiters and column names) to a specific csv file.
 */
public interface CSVRules {

    /**
     * This method get all the fields of this class (by reflection) and maps each field with it's corresponding value.
     * The returned map has a key set with the value specified by the class fields, each of these values represents the name of
     * a column in the csv file, the name of the constant (field) correspond to the actual name of the column in the database. (see the entities classes)
     * Example: "Produto" is the column specified by the csv file, this column represents the PRODUCT_NAME field of an entity of the database.
     * This way we can assign the values of this specific column of the csv file to it's corresponding field of the specific table.
     * @return
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     */
    public HashMap<String, String> getFieldsMap() throws ClassNotFoundException, IllegalAccessException;

    public String getDelimiter();

}

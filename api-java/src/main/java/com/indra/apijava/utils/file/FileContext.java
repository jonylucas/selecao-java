package com.indra.apijava.utils.file;

import com.indra.apijava.utils.file.rules.TypeRulesOption;
import com.indra.apijava.utils.file.rules.csv.AnpRules;
import com.indra.apijava.utils.file.rules.csv.CSVRulesOption;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.indra.apijava.utils.file.rules.csv.CSVRulesOption.ANP;
import static com.indra.apijava.utils.file.rules.csv.CSVRulesOption.STANDARD;

/***
 * @author Joao
 * This is the context class for handling the file operations (Strategy Pattern).
 * It can be expanded for another file extensions.
 */
public class FileContext {

    private static FileHandler handler = new CSVHandler(); // CSV is the default file type for this system
    private static MultipartFile file;

    /**
     * Gets the file name anc check the extension of this file, to determine which type of handler will operate this file.
     * Example: If the type of file is .xml, a xml file handler (not implemented) will take care of this file.
     * @param multipartFile
     */
    public static void setHandlerByFile(MultipartFile multipartFile){
        file = multipartFile;
        if(file != null && Objects.equals(file.getContentType(), "text/csv")) {
            handler = new CSVHandler();
        }
    }

    /**
     * Import a file with a specific set of rules
     * @param setRules
     * @return
     */
    public static Map<String, List<String>> importFile(String setRules){
        if(file == null){
            return new HashMap<>(); //The return of an empty map prevents NPE.
        }
        setRulesByHandleType(setRules);
        return handler.importFile(file);
    }

    private static void setRulesByHandleType(String setRules){
        TypeRulesOption rulesOption;
        try{
            if(CSVHandler.class.isInstance(handler)){
                rulesOption = CSVRulesOption.valueOf(setRules);

                if (CSVRulesOption.ANP.equals(rulesOption)) {
                    handler.setRules(AnpRules.getInstance());
                } else {
                    System.out.println("Not yet Implemented"); // Others set of rules (such as default) are not implemented.
                }
            }else {
                throw new Exception("The file type is not supported");
            }
        }catch (IllegalArgumentException iae){
            handler.setRules(AnpRules.getInstance()); // The default set of rules (for csv).
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}

package com.indra.apijava.utils.file.rules;

/**
 * @author Joao
 * Just a mark interface to define the TypeRulesOption for each type of file
 */
public interface TypeRulesOption {}

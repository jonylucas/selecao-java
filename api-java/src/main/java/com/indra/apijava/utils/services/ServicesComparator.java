package com.indra.apijava.utils.services;

import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;

import java.util.Comparator;

/**
 * This class is used to sort a collection of ServiceInterface
 */
public class ServicesComparator implements Comparator<ServiceInterface> {

    @Override
    public int compare(ServiceInterface o1, ServiceInterface o2) {
        ServicePriority i = o1.getPriority();
        ServicePriority j = o2.getPriority();

        if(i == j){
            return 0;
        }else if(i.getPriorityValue() > j.getPriorityValue()){
            return 1;
        }else {
            return -1;
        }
    }
}

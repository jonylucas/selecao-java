package com.indra.apijava.utils.file.rules.csv;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * @author Joao
 * This class specify the csv template, obtained in a csv file provided by the "Agencia Nacional de Petroleo" (ANP).
 * Also, this class maps the csv fields with the table attributes.
 */
public final class AnpRules implements CSVRules{
    private static AnpRules instance;

    private final String DELIMITER = "  ";
    private final String CITY_NAME = "Município";
    private final String MEASURE_UNIT = "Unidade de Medida";
    private final String COLLECTING_DATE = "Data da Coleta";
    private final String BRAND_NAME = "Bandeira";
    private final String STATE_ACRONYM = "Estado - Sigla";
    private final String REGION_ACRONYM = "Região - Sigla";
    private final String ESTABLISHMENT_CODE = "Instalação - Código";
    private final String BUYING_VALUE = "Valor de Compra";
    private final String ESTABLISHMENT_NAME = "Revenda";
    private final String PRODUCT_NAME = "Produto";
    private final String SELLING_VALUE = "Valor de Venda";

    private HashMap<String, String> fieldsMap = new HashMap<>();

    /**
     * private standard constructor (this class can't be instantiated anywhere).
     */
    private AnpRules(){}

    /**
     * Return the singleton
     * @return
     */
    public static AnpRules getInstance(){
        if(instance == null){
            instance = new AnpRules();
        }
        return instance;
    }

    @Override
    public HashMap<String, String> getFieldsMap() throws ClassNotFoundException, IllegalAccessException {
        if(fieldsMap.isEmpty()){
            Class info = Class.forName(this.getClass().getName());
            Field[] declaredFields = info.getDeclaredFields();
            for(Field field : declaredFields){
                fieldsMap.put(String.valueOf(field.get(this)), field.getName().toLowerCase());
            }
        }

//        for(String key : fieldsMap.keySet()){
//            System.out.println("Field: " + fieldsMap.get(key) + " - Value: " + key);
//        }

        return fieldsMap;
    }

    @Override
    public String getDelimiter() {
        return DELIMITER;
    }

}

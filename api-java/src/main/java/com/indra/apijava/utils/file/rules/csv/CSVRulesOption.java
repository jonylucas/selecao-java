package com.indra.apijava.utils.file.rules.csv;

import com.indra.apijava.utils.file.rules.TypeRulesOption;

public enum CSVRulesOption implements TypeRulesOption {
    STANDARD,
    ANP
}

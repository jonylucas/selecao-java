package com.indra.apijava.utils.file;

import com.indra.apijava.utils.file.rules.csv.AnpRules;
import com.indra.apijava.utils.file.rules.csv.CSVRules;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

/**
 * @author Joao
 * This class is responsible to read a csv file and convert it to a HashMap with entities fields of the database and
 * it's corresponding value (present in the csv file).
 */
class CSVHandler implements FileHandler {

    // The set of rules necessary to read and import the values of the csv value.
    private CSVRules rules = AnpRules.getInstance();

    @Override
    public void setRules(CSVRules rules){
        this.rules = rules;
    }

    @Override
    public Map<String,List<String>> importFile(MultipartFile multipartFile) {

        Map<String, List<String>> csvValues = new HashMap<>();

        try (Scanner scan = new Scanner(multipartFile.getInputStream())){

            String delimiter = rules.getDelimiter();
            Map<String, String> fieldMaps = rules.getFieldsMap();
            List<String> fieldsList = Arrays.asList(scan.nextLine().split(delimiter));

            //Initialize the hashmap
            for (String field : fieldsList){
                csvValues.put(fieldMaps.get(field), new ArrayList<>());
            }

            while (scan.hasNext()){
                String[] tokens = scan.nextLine().split(delimiter);
                if(tokens.length != fieldsList.size()) // skip the invalid lines (lines with more/less columns)
                    continue;
                int index = 0;
                for (String field : fieldsList){
                    tokens[index] = tokens[index].replace(",", "."); // replace ',' for '.' to be used in Double casting
                    csvValues.get(fieldMaps.get(field)).add(tokens[index++]);
                }
            }

//            csvValues.forEach((x, y) -> {
//                System.out.println(x + " - " + y);
//            });

        } catch (IOException | IllegalAccessException | ClassNotFoundException  e) {
            System.out.println("File import failed: " + e.getMessage() );
        }catch (Exception e){
            System.out.println("Unknow error");
        }

        return csvValues;

    }
}

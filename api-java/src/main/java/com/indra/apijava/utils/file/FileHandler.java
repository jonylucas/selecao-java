package com.indra.apijava.utils.file;

import com.indra.apijava.utils.file.rules.csv.CSVRules;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author Joao
 * This interface it's used to mark classes that can read and import data from a file.
 * NOTE: Only csv files are support (for now), but it's possible to extend the support for other file types.
 */
interface FileHandler {

    /**
     * This method receives a file and return the data imported from this file in the form of a map.
     * The keys of this map correspond to entities table's columns, and each key is mapped to a list (value)
     * that contains the values extracted from the file.
     * @param multipartFile
     * @return
     */
    Map<String, List<String>> importFile(MultipartFile multipartFile);

    public void setRules(CSVRules rules);
}

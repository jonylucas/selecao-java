package com.indra.apijava.service.entity;

import com.indra.apijava.data.entity.City;
import com.indra.apijava.data.entity.State;
import com.indra.apijava.data.repository.CityRepository;
import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("city")
public class CityService implements ServiceInterface<City, Long> {

    private final ServicePriority priority = ServicePriority.MODERATE_PRIORITY;

    private final CityRepository cityRepository;

    @Autowired
    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public City get(Long id) {
        Optional<City> response = cityRepository.findById(id);
        return response.orElse(null);
    }

    public City getByName(String name){
        return cityRepository.findByName(name);
    }

    @Override
    public List<City> getAll() {
        List<City> cities = new ArrayList<>();
        cityRepository.findAll().forEach(cities::add);
        return cities;
    }

    @Override
    public void create(City city) {
        City cityInstance = cityRepository.findByName(city.getName());
        if(cityInstance == null){
            cityRepository.save(city);
        }
    }

    @Override
    public void createByJson(JSONObject jsonObject, List<ServiceInterface> services) {
        City city = new City();
        city.setName(jsonObject.getString("city_name"));
        services.forEach(x -> {
            if (x.getQualifier().equals("state")){
                String stateAcronym = jsonObject.getString("state_acronym");
                State state = ((StateService) x).getByAcronym(stateAcronym);
                city.setState(state);
            }
        });
        create(city);
    }

    @Override
    public void delete(City city) {
        Optional<City> response = cityRepository.findById(city.getId());
        response.ifPresent(cityRepository::delete);
    }

    @Override
    public void update(City target) {
        Optional<City> response = cityRepository.findById(target.getId());
        if(response.isPresent()){
            City city = response.get();
            city.setName(target.getName());
            city.setState(target.getState());
            cityRepository.save(city);
        }
    }

    @Override
    public String getQualifier() {
        return "city";
    }

    @Override
    public ServicePriority getPriority() {
        return this.priority;
    }

}

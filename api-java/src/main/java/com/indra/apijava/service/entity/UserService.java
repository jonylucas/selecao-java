package com.indra.apijava.service.entity;

import com.indra.apijava.data.entity.User;
import com.indra.apijava.data.repository.UserRepository;
import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("user")
public class UserService implements ServiceInterface<User, Long> {

    private final ServicePriority priority = ServicePriority.LOW_PRIORITY;

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User get(Long id) {
        Optional<User> userResponse = userRepository.findById(id);
        return userResponse.orElse(null);
    }

    @Override
    public List<User> getAll() {
        List<User> usersList = new ArrayList<>();
        userRepository.findAll().forEach(usersList::add);
        return usersList;
    }

    @Override
    public void create(User user) {
        userRepository.save(user);
    }

    @Override
    public void createByJson(JSONObject jsonObject, List<ServiceInterface> services) {
        User user = new User();
        user.setName(jsonObject.getString("user_name"));
        user.setLogin(jsonObject.getString("user_login"));
        user.setPassword(jsonObject.getString("user_password"));
        create(user);
    }

    @Override
    public void delete(User user) {
        Optional<User> userResponse = userRepository.findById(user.getId());
        userResponse.ifPresent(userRepository::delete);
    }

    @Override
    public void update(User target) {
        Optional<User> userResponse = userRepository.findById(target.getId());
        if(userResponse.isPresent()){
            User user = userResponse.get();
            user.setName(target.getName());
            user.setLogin(target.getLogin());
            user.setPassword(target.getPassword());
            userRepository.save(user);
        }
    }

    @Override
    public String getQualifier() {
        return "user";
    }

    @Override
    public ServicePriority getPriority() {
        return this.priority;
    }


}

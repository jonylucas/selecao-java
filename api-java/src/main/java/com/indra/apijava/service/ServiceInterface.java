package com.indra.apijava.service;

import org.json.JSONObject;

import java.util.List;

public interface ServiceInterface <T, V> {
    T get(V v);
    List<T> getAll();
    void create(T t);
    void createByJson(JSONObject jsonObject, List<ServiceInterface> services);
    void delete(T t);
    void update(T t);
    String getQualifier();
    ServicePriority getPriority();

}

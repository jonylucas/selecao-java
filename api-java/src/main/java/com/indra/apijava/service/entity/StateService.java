package com.indra.apijava.service.entity;

import com.indra.apijava.data.entity.Region;
import com.indra.apijava.data.entity.State;
import com.indra.apijava.data.repository.StateRepository;
import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("state")
public class StateService implements ServiceInterface<State, Long> {

    private final ServicePriority priority = ServicePriority.HIGH_PRIORITY;

    private final StateRepository stateRepository;

    @Autowired
    public StateService(StateRepository stateRepository) { this.stateRepository = stateRepository; }

    @Override
    public State get(Long id) {
        Optional<State> stateOptional = stateRepository.findById(id);
        return stateOptional.orElse(null);
    }

    public State getByAcronym(String acronym){
        return stateRepository.findByStateAcronym(acronym);
    }

    @Override
    public List<State> getAll() {
        List<State> states = new ArrayList<>();
        stateRepository.findAll().forEach(states::add);
        return states;
    }

    @Override
    public void create(State state) {
        // The state acronym is unique, so it's necessary to check if it's already exist.
        State instance = stateRepository.findByStateAcronym(state.getStateAcronym());
        if(instance == null){
            stateRepository.save(state);
        }
    }

    @Override
    public void createByJson(JSONObject jsonObject, List<ServiceInterface> services) {
        State state = new State();
        state.setStateAcronym(jsonObject.getString("state_acronym"));

        services.forEach(x -> {
            if (x.getQualifier().equals("region")){
                String regionAcronym = jsonObject.getString("region_acronym");
                Region region = ((RegionService) x).getByAcronym(regionAcronym);
                state.setRegion(region);
            }
        });

        create(state);

    }

    @Override
    public void delete(State state) {
        Optional<State> stateOptional = stateRepository.findById(state.getId());
        stateOptional.ifPresent(stateRepository::delete);
    }

    @Override
    public void update(State target) {
        Optional<State> response = stateRepository.findById(target.getId());
        if(response.isPresent()){
            State state = response.get();
            state.setStateAcronym(target.getStateAcronym());
            state.setRegion(target.getRegion());
            stateRepository.save(state);
        }
    }

    @Override
    public String getQualifier() { return "state"; }

    @Override
    public ServicePriority getPriority() { return this.priority; }


}

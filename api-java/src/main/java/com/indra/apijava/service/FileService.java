package com.indra.apijava.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.indra.apijava.utils.file.FileContext;
import com.indra.apijava.utils.services.ServicesComparator;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class is responsible to pass the file to a suitable FileHandler (for read the file),
 * And import it's data to the database (create entities instances).
 */
@Service
public class FileService {

    // A map for all qualifiers and its respective service.
    private Map<String, ServiceInterface> serviceByQualifier = new HashMap<>();

    // Includes all services that won't be used in the file import process.
    private final String[] excludeQualifiers = new String[] {"user"};

    @Autowired
    public FileService(List<ServiceInterface> services){
        for (ServiceInterface service : services){
            serviceByQualifier.put(service.getQualifier(), service);
        }
    }

    public void importData(MultipartFile file){
        importData(file, "ANP");
    }

    /**
     * This method sends the file to a suitable FileHandler and import it's data.
     * @param file
     * @param setRules
     */
    public void importData(MultipartFile file, String setRules){
        Map<String, List<String>> fieldMap;
        FileContext.setHandlerByFile(file);
        fieldMap = FileContext.importFile(setRules);
        createInstances(fieldMap);
    }

    /**
     * Creates entities instances based on the data extracted from the file.
     * @param map
     */
    private void createInstances(Map<String, List<String>> map){
        ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
        JSONObject[] result = formatToJson(map);

        List<String> excludedServices = Arrays.asList(excludeQualifiers);
        List<ServiceInterface> services = new ArrayList<>(serviceByQualifier.values());
        Collections.sort(services, new ServicesComparator().reversed()); // Sorting the array in descending order of priority

        for(JSONObject jsonObject : result){
            for (ServiceInterface service : services){
                if(!excludedServices.contains(service.getQualifier()))
                    service.createByJson(jsonObject, services);
            }
        }

//        for(ServiceInterface service : services){
//            if(!excludedServices.contains(service.getQualifier()))
//                service.getAll().forEach(x -> System.out.println(x));
//        }

    }

    /**
     * This method receives a map (field, values) and returns a list of Json Objects.
     * The key of the map correspond to the fields of the json,
     * And the values is the respective value of the field.
     * @param map
     * @return
     */
    private JSONObject[] formatToJson(Map<String, List<String>> map){
        int size = 0; // Number of instances
        int mapSize = map.keySet().size(); // Number of keys (fields)
        for(String key : map.keySet()){
            size = map.get(key).size();
            break;
        }

        String[] eachObject = new String[size];
        JSONObject[] jsonObjects = new JSONObject[size];
        Arrays.fill(eachObject, "{");

        int keyIndex = 0;
        for(String key : map.keySet()){
            List<String> values = map.get(key);
            for(int i = 0; i < size; i++){
                if(keyIndex < mapSize-1){
                    eachObject[i] += "\"" + key + "\": \"" + values.get(i) + "\", ";
                }else {
                    eachObject[i] += "\"" + key + "\": \"" + values.get(i) + "\"}";
                    jsonObjects[i] = new JSONObject(eachObject[i]);
                }
            }
            keyIndex++;
        }

        return jsonObjects;

    }

}

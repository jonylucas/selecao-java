package com.indra.apijava.service.entity;

import com.indra.apijava.data.entity.Region;
import com.indra.apijava.data.repository.RegionRepository;
import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("region")
public class RegionService implements ServiceInterface<Region, Long> {

    private final ServicePriority priority = ServicePriority.VERY_HIGH_PRIORITY;

    private final RegionRepository regionRepository;

    @Autowired
    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    @Override
    public Region get(Long id) {
        Optional<Region> regionOptional = regionRepository.findById(id);
        return regionOptional.orElse(null);
    }

    @Override
    public List<Region> getAll() {
        List<Region> regions = new ArrayList<>();
        regionRepository.findAll().forEach(regions::add);
        return regions;
    }

    public Region getByAcronym(String acronym) {
        return regionRepository.findByRegionAcronym(acronym);
    }

    @Override
    public void create(Region region) {
        // The region acronym is unique, so it's necessary to check if it's already exist.
        Region regionInstance = regionRepository.findByRegionAcronym(region.getRegionAcronym());
        if(regionInstance == null){
            regionRepository.save(region);
        }
    }

    @Override
    public void createByJson(JSONObject jsonObject, List<ServiceInterface> services) {
        Region region = new Region();
        region.setRegionAcronym(jsonObject.getString("region_acronym"));
        create(region);
    }

    @Override
    public void delete(Region region) {
        Optional<Region> regionOptional = regionRepository.findById(region.getId());
        regionOptional.ifPresent(regionRepository::delete);
    }

    @Override
    public void update(Region target) {
        Optional<Region> response = regionRepository.findById(target.getId());
        if(response.isPresent()){
            Region region = response.get();
            region.setRegionAcronym(target.getRegionAcronym());
            regionRepository.save(region);
        }
    }

    @Override
    public String getQualifier() {
        return "region";
    }

    @Override
    public ServicePriority getPriority() {
        return this.priority;
    }

}

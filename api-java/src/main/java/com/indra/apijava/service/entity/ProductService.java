package com.indra.apijava.service.entity;

import com.indra.apijava.data.entity.Establishment;
import com.indra.apijava.data.entity.Product;
import com.indra.apijava.data.repository.ProductRepository;
import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service(value = "product")
public class ProductService implements ServiceInterface<Product, Long> {

    private final ServicePriority priority = ServicePriority.LOW_PRIORITY;

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product get(Long id) {
        Optional<Product> productResponse = productRepository.findById(id);
        return productResponse.orElse(null);
    }

    public Product groupByBrand(String brandName){
        return productRepository.findDistinctFirstByBrandName(brandName);
    }

    public Product groupByDate(Product product){
        Date date = product.getCollectingDate();
        return productRepository.findDistinctFirstByCollectingDate(new java.sql.Date(date.getTime()));
    }

    @Override
    public List<Product> getAll() {
        List<Product> productList = new ArrayList<>();
        productRepository.findAll().forEach(productList::add);
        return productList;
    }

    public List<Product> getAllByEstablishmentId(long code){
        return productRepository.findAllByEstablishment_Code(code);
    }

    @Override
    public void create(Product product) {
        productRepository.save(product);
    }

    @Override
    public void createByJson(JSONObject jsonObject, List<ServiceInterface> services) {
        Product product = new Product();
        product.setName(jsonObject.getString("product_name"));

        String doubleValue = jsonObject.getString("buying_value");
        double buyingValue = !doubleValue.equals("") ? Double.parseDouble(doubleValue) : 0;
        product.setBuyingValue(buyingValue);

        doubleValue = jsonObject.getString("selling_value");
        double sellingValue = !doubleValue.equals("") ? Double.parseDouble(doubleValue) : 0;
        product.setSellingValue(sellingValue);

        product.setMeasureUnit(jsonObject.getString("measure_unit"));
        product.setBrandName(jsonObject.getString("brand_name"));

        services.forEach(x -> {
            if (x.getQualifier().equals("establishment")){
                long establishmentCode = jsonObject.getLong("establishment_code");
                Establishment establishment = ((EstablishmentService) x).getByCode(establishmentCode);
                product.setEstablishment(establishment);
            }
        });

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            product.setCollectingDate(format.parse(jsonObject.getString("collecting_date")));
        } catch (ParseException e) {
            System.out.println("Date format Error");
        }

        create(product);
    }

    @Override
    public void delete(Product product) {

    }

    @Override
    public void update(Product target) {
        Optional<Product> response = productRepository.findById(target.getId());
        if(response.isPresent()){
            Product product = response.get();
            product.setName(target.getName());
            product.setSellingValue(target.getSellingValue());
            product.setBuyingValue(target.getBuyingValue());
            product.setMeasureUnit(target.getMeasureUnit());
            product.setCollectingDate(target.getCollectingDate());
            product.setEstablishment(target.getEstablishment());
            product.setBrandName(target.getBrandName());
            productRepository.save(product);
        }
    }

    @Override
    public String getQualifier() {
        return "product";
    }

    @Override
    public ServicePriority getPriority() {
        return this.priority;
    }

}
package com.indra.apijava.service.entity;

import com.indra.apijava.data.entity.City;
import com.indra.apijava.data.entity.Establishment;
import com.indra.apijava.data.repository.EstablishmentRepository;
import com.indra.apijava.service.ServiceInterface;
import com.indra.apijava.service.ServicePriority;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("establishment")
public class EstablishmentService implements ServiceInterface<Establishment, Long> {

    private final ServicePriority priority = ServicePriority.MODERATE_PRIORITY;

    private final EstablishmentRepository establishmentRepository;

    @Autowired
    public EstablishmentService(EstablishmentRepository establishmentRepository) {
        this.establishmentRepository = establishmentRepository;
    }

    @Override
    public Establishment get(Long id) {
        Optional<Establishment> establishmentResponse = establishmentRepository.findById(id);
        return establishmentResponse.orElse(null);
    }

    public Establishment getByCode(long code) {
        return establishmentRepository.findByCode(code);
    }

    @Override
    public List<Establishment> getAll() {
        List<Establishment> establishments = new ArrayList<>();
        establishmentRepository.findAll().forEach(establishments::add);
        return establishments;
    }

    public List<Establishment> getByCityName(String cityName){
        return establishmentRepository.findAllByCity_Name(cityName);
    }

    @Override
    public void create(Establishment establishment) {
        Establishment response = establishmentRepository.findByCode(establishment.getCode());
        if(response == null)
            establishmentRepository.save(establishment);
    }

    @Override
    public void createByJson(JSONObject jsonObject, List<ServiceInterface> services) {
        Establishment establishment = new Establishment();
        establishment.setCode( Long.parseLong(jsonObject.getString("establishment_code")) );
        establishment.setName(jsonObject.getString("establishment_name"));

        services.forEach(x -> {
            if (x.getQualifier().equals("city")){
                String cityName = jsonObject.getString("city_name");
                City city = ((CityService) x).getByName(cityName);
                establishment.setCity(city);
            }
        });
        create(establishment);
    }

    @Override
    public void delete(Establishment establishment) {
        Optional<Establishment> response = establishmentRepository.findById(establishment.getId());
        response.ifPresent(establishmentRepository::delete);
    }

    @Override
    public void update(Establishment target) {
        Optional<Establishment> response = establishmentRepository.findById(target.getId());
        if(response.isPresent()){
            Establishment establishment = response.get();
            establishment.setName(target.getName());
            establishment.setCity(target.getCity());
            establishmentRepository.save(establishment);
        }
    }

    @Override
    public String getQualifier() {
        return "establishment";
    }

    @Override
    public ServicePriority getPriority() {
        return this.priority;
    }

}

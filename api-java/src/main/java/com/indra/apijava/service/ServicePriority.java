package com.indra.apijava.service;

/**
 * This enum specify the priority order for the services that implements Service interface, and it's utilized to define
 * a order to create instances of entities, starting with the entities with higher scales (scope).
 * e.g.: Country -> Regions -> States -> Cities -> District
 * This way, lower scale entities can reference to it's parent's fields.
 * Hint:
 *      - Very high priority service should be used for create entities that have many OneToMany relationships.
 *      - High priority should be used for entities that have ManyToOne and OneToMany relationships.
 *      - Moderate priority should be used for entities that have ManyToOne and/or ManyToMany relationships.
 *      - Low priority should be used for entities that are isolated (don't have relations with others entities).
 *
 */
public enum ServicePriority implements Comparable<ServicePriority> {
    VERY_HIGH_PRIORITY(4),
    HIGH_PRIORITY(3),
    MODERATE_PRIORITY(2),
    LOW_PRIORITY(1);

    private int priorityOrder;

    ServicePriority(int priorityOrder){
        this.priorityOrder = priorityOrder;
    }

    public int getPriorityValue(){
        return this.priorityOrder;
    }

}

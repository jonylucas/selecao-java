package com.indra.apijava.service;

import com.indra.apijava.data.entity.City;
import com.indra.apijava.data.entity.Establishment;
import com.indra.apijava.data.entity.Product;
import com.indra.apijava.data.entity.Region;
import com.indra.apijava.service.entity.EstablishmentService;
import com.indra.apijava.service.entity.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class DataTransferService {

    private List<ServiceInterface> services;

    @Autowired
    public  DataTransferService(ServiceInterface ... args){
        services = new ArrayList<>();
        for (ServiceInterface arg : args){
            services.add(arg);
        }
    }

    private ServiceInterface getService(String type){
        for(ServiceInterface service : services){
            if(service.getQualifier().equals(type))
                return service;
        }

        return null; // Should be careful with this
    }

    // Specific query to return the productPriceHistory
    public List<String> getProductPriceHistory(){

        ServiceInterface service = getService("product");
        List<String> response = new ArrayList<>();
        service.getAll().forEach( x -> {
            Product product = (Product) x;
            String result = "{";
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            result += "\"date\": \"" + format.format(product.getCollectingDate()) + "\", ";
            result += "\"selling_price\": " + product.getSellingValue() + " " + product.getMeasureUnit() + "}";
            response.add(result);
        });
        return response;
    }

    // Specific query to return the average fuel price by city name
    public String getAvgPriceByCityName(String cityName){

        ServiceInterface service = getService("establishment");
        List<Establishment> establishmentList = ((EstablishmentService) service).getByCityName(cityName);

        double sum = 0;
        int size = 0;
        String metricUnit = "";

        service = getService("product");
        List<Product> products = service.getAll();

        for (Product product : products){
            if(establishmentList.contains(product.getEstablishment())){
                sum += product.getSellingValue();
                metricUnit = product.getMeasureUnit();
                size++;
            }
        }

        return String.format("Average price in " + cityName +": %.3f " + metricUnit, sum/size);

    }

    // Specif query to all information by the region acronym
    public List<String> getRegionInfo(String regionAcronym){
        ServiceInterface service = getService("product"); // Product is the entity that's possible to access all the information needed.
        List<Product> products = service.getAll();
        List<String> result = new ArrayList<>();


        for (Product product : products){
            Region region = product.getEstablishment()
                    .getCity()
                    .getState()
                    .getRegion();

            if(region.getRegionAcronym().equals(regionAcronym)){
                result.add(getDataDescription(product));
            }
        }

        return result;

    }

    // Get information by the brand_name
    public List<String> getGroupByBrand(){
        ServiceInterface service = getService("product");

        List<String> percorredBrandNames = new ArrayList<>();
        List<String> result = new ArrayList<>();
        List<Product> products = service.getAll();
        Product response = products.get(0);

        for(Product product : products){
            if(!percorredBrandNames.contains(product.getBrandName())){
                response = ((ProductService) service).groupByBrand(product.getBrandName());
                percorredBrandNames.add(product.getBrandName());
                result.add(getDataDescription(response));
            }
        }

        return result;

    }

    // Get group information by the collecting date
    public List<String> getGroupByCollectingDate(){
        ServiceInterface service = getService("product");

        List<Date> percorredDateList = new ArrayList<>();
        List<String> result = new ArrayList<>();
        List<Product> products = service.getAll();
        Product response = products.get(0);

        for(Product product : products){
            if(!percorredDateList.contains(product.getCollectingDate())){
                response = ((ProductService) service).groupByDate(product);
                percorredDateList.add(product.getCollectingDate());
                result.add(getDataDescription(response));
            }
        }

        return result;

    }

    // Get average selling and buying price by city
    public List<String> getAvgPriceForEachCity(){
        ServiceInterface service = getService("product");

        HashMap<String, Double> cityBuyingValue = new HashMap<>(); // A map to the city name and the average buying value
        HashMap<String, Double> citySellinggValue = new HashMap<>(); // A map to the city name and the average selling value

        HashMap<String, Integer> sellingSize = new HashMap<>(); // A map to the city name and the number of it's selling value occurrence
        HashMap<String, Integer> buyingSize = new HashMap<>(); // A map to the city name and the number of it's buying value occurrence

        List<Product> products = ((ProductService)service).getAll();
        City city;

        for(Product product : products){
            city = product.getEstablishment().getCity();
            if(citySellinggValue.keySet().contains(city.getName())){
                double currentValue = citySellinggValue.get(city.getName()) + product.getSellingValue();
                citySellinggValue.put(city.getName(), currentValue);
                sellingSize.put(city.getName(), sellingSize.get(city.getName()) + 1);

                if(product.getBuyingValue() > 0){
                    currentValue = cityBuyingValue.get(city.getName()) + product.getBuyingValue();
                    cityBuyingValue.put(city.getName(), currentValue);
                    buyingSize.put(city.getName(), buyingSize.get(city.getName()) + 1);
                }

            }else{
                citySellinggValue.put(city.getName(), product.getSellingValue());
                cityBuyingValue.put(city.getName(), product.getBuyingValue());
                sellingSize.put(city.getName(), 1);
                buyingSize.put(city.getName(), 1);
            }
        }

        List<String> result = new ArrayList<>();

        for (String cityName : citySellinggValue.keySet()){
            double avgSelling = citySellinggValue.get(cityName)/sellingSize.get(cityName);
            double avgBuying = cityBuyingValue.get(cityName)/buyingSize.get(cityName);

            String sellingAvg = String.format("%.3f", avgSelling);
            String buyingAvg = String.format("%.3f", avgBuying);

            result.add("{ city_name: " + cityName + ", avg_selling_value: " +
                    sellingAvg + ", avg_buying_value: " + buyingAvg + "}");
        }

        return result;
    }

    // Get average selling and buying price by brand
    public List<String> getAvgPriceForEachBrand(){
        ServiceInterface service = getService("product");

        HashMap<String, Double> brandBuyingValue = new HashMap<>(); // A map to the brand name and the average buying value
        HashMap<String, Double> brandSellingValue = new HashMap<>(); // A map to the brand name and the average selling value
        List<Product> products = service.getAll();

        HashMap<String, Integer> sellingSize = new HashMap<>(); // A map to the brand name and the number of it's selling value occurrence
        HashMap<String, Integer> buyingSize = new HashMap<>(); // A map to the brand name and the number of it's buying value occurrence

        for(Product product : products){
            if(brandSellingValue.keySet().contains(product.getBrandName())){
                double currentValue = brandSellingValue.get(product.getBrandName()) + product.getSellingValue();
                brandSellingValue.put(product.getBrandName(), currentValue);
                sellingSize.put(product.getBrandName(), sellingSize.get(product.getBrandName()) + 1);

                if(product.getBuyingValue() > 0){
                    currentValue = brandBuyingValue.get(product.getBrandName()) + product.getBuyingValue();
                    brandBuyingValue.put(product.getBrandName(), currentValue);
                    buyingSize.put(product.getBrandName(), buyingSize.get(product.getBrandName()) + 1);
                }
            }else{
                brandSellingValue.put(product.getBrandName(), product.getSellingValue());
                brandBuyingValue.put(product.getBrandName(), product.getBuyingValue());
                sellingSize.put(product.getBrandName(), 1);
                buyingSize.put(product.getBrandName(), 1);
            }
        }

        List<String> result = new ArrayList<>();



        for (String brandName : brandSellingValue.keySet()){
            double avgSelling = brandSellingValue.get(brandName)/sellingSize.get(brandName);
            double avgBuying = brandBuyingValue.get(brandName)/buyingSize.get(brandName);

            String sellingAvg = String.format("%.3f", avgSelling);
            String buyingAvg = String.format("%.3f", avgBuying);

            result.add("{ brand_name: " + brandName + ", avg_selling_value: " +
                    sellingAvg + ", avg_buying_value: " + buyingAvg + "}");
        }


        return result;
    }

    private String getDataDescription(Product product){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        String dataDescription = ("{" +
                "product_name: " + product.getName() + ", selling value: " + product.getSellingValue() +
                ", collection_date: " + format.format(product.getCollectingDate()) + ", brand_name: " + product.getBrandName() + ", " +
                "establishment: " + product.getEstablishment().getName() + ", " +
                "city_name: " + product.getEstablishment().getCity().getName() + ", " +
                "state_acronym: " + product.getEstablishment().getCity().getState().getStateAcronym() + ", " +
                "region_acronym: " + product.getEstablishment().getCity().getState().getStateAcronym() +
                "}");
        return dataDescription;
    }


}

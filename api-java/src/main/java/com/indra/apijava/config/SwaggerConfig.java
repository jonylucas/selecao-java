package com.indra.apijava.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage("com.indra.apijava"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo(){
        return new ApiInfo(
                "Seleção Indra: Java developer",
                "REST Api, desenvolvida com Spring Boot, para o processo seletivo da vaga de desenvolvedor java",
                "1.0",
                "Uso livre (sem autenticação)",
                new springfox.documentation.service.Contact("João Lucas Fabião Amorim", "http://linkedin.com/in/jo%C3%A3o-lucas-fabi%C3%A3o-amorim-403234109", "jony.lucas.br13@gmail.com"),
                "Spring Boot license",
                "https://github.com/spring-projects/spring-boot/blob/master/LICENSE.txt",
                Collections.emptyList()
        );
    }

}

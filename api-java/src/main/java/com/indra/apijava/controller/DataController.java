package com.indra.apijava.controller;

import com.indra.apijava.service.DataTransferService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/data")
@CrossOrigin(origins = "http://localhost:4200")
public class DataController {

    private final DataTransferService dataService;

    @Autowired
    public DataController(DataTransferService dataService) {
        this.dataService = dataService;
    }

    @GetMapping(path = "avg-price-by-city/{name}")
    @ApiOperation(
            value = "Retorna a média de preço de combustível com base no nome do município",
            notes = "Forneça o nome da cidade que você procura",
            response = String.class
    )
    @ResponseBody
    public String getAvgPriceByCityName(@PathVariable("name") String cityName){
        return dataService.getAvgPriceByCityName(cityName);
    }

    @GetMapping(path = "/price-history")
    @ApiOperation(value = "Retorna o histórico de preços de combustível")
    @ResponseBody
    public List<String> getPriceHistory(){
        return dataService.getProductPriceHistory();
    }

    @GetMapping(path = "/by-region/{acronym}")
    @ApiOperation(value = "retorna todas as informações importadas por sigla da região", notes = "Forneça a sigla do estado que você procura")
    @ResponseBody
    public List<String> getRegionInfo(@PathVariable("acronym") @ApiParam(value = "A sigla do estado", required = true) String regionAcronym){
        return dataService.getRegionInfo(regionAcronym);
    }

    @GetMapping(path = "/groupby-date")
    @ApiOperation(value = "Retorna os dados agrupados pela data da coleta")
    @ResponseBody
    public List<String> getGroupByDate(){
        return dataService.getGroupByCollectingDate();
    }

    @GetMapping(path = "/groupby-brand")
    @ApiOperation(value = "Retorna os dados agrupados por distribuidora")
    @ResponseBody
    public List<String> getGroupByBrand(){
        return dataService.getGroupByBrand();
    }

    @GetMapping(path = "/avg-price-groupby-city")
    @ApiOperation(value = "retorna o valor médio do valor da compra e do valor da venda por município")
    @ResponseBody
    public List<String> getAvgPriceForEachCity(){
        return dataService.getAvgPriceForEachCity();
    }

    @GetMapping(path = "/avg-price-groupby-brand")
    @ApiOperation(value = "retorne o valor médio do valor da compra e do valor da venda por bandeira")
    @ResponseBody
    public List<String> getAvgPriceForEachBrand(){
        return dataService.getAvgPriceForEachBrand();
    }


}

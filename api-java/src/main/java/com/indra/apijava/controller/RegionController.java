package com.indra.apijava.controller;

import com.indra.apijava.data.entity.Region;
import com.indra.apijava.data.entity.State;
import com.indra.apijava.service.entity.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/region")
public class RegionController {

    private final RegionService regionService;

    @Autowired
    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Region> getAllRegions(){
        List<Region> regions = regionService.getAll();
        return regionService.getAll();
    }

    @PostMapping
    public void createRegion(@RequestBody Region region){
        regionService.create(region);
    }

    @GetMapping(path = "{id}")
    public Region getRegionById(@PathVariable(name = "id") long id){
        return regionService.get(id);
    }

    @DeleteMapping
    public void deleteRegion(@RequestBody Region region){
        regionService.delete(region);
    }

    @PutMapping
    public void updateRegion(@RequestBody Region region){
        regionService.update(region);
    }

}

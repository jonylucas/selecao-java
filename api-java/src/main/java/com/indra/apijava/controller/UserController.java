package com.indra.apijava.controller;

import com.indra.apijava.data.entity.User;
import com.indra.apijava.service.entity.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ApiOperation(
            value = "Retorna todos os usuários do sistema",
            response = List.class
    )
    public List<User> getAllUsers(){
        return userService.getAll();
    }

    @PostMapping
    public void createUser(@RequestBody User user){
        userService.create(user);
    }

    @GetMapping(path = "{id}")
    public User getUserById(@PathVariable(name = "id") long id){
        return userService.get(id);
    }

    @DeleteMapping
    public void deleteUser(@RequestBody User user){
        userService.delete(user);
    }

    @PutMapping
    public void updateUser(@Valid @RequestBody User user){
        userService.update(user);
    }
}

package com.indra.apijava.controller;

import com.indra.apijava.data.entity.Product;
import com.indra.apijava.service.entity.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/product")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<Product> getAllProducts(){
        return productService.getAll();
    }

    @PostMapping
    public void createProduct(@RequestBody Product product){
        productService.create(product);
    }

}

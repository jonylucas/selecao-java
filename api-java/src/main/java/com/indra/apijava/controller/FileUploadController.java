package com.indra.apijava.controller;

import com.indra.apijava.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.synchronoss.cloud.nio.multipart.Multipart;

/***
 * POST - This class is responsible to get a file from a POST request and send it to the FileService to handle this file.
 */
@RestController
@RequestMapping("api/upload")
public class FileUploadController {

    private final FileService fileService;

    @Autowired
    public FileUploadController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping
    public void uploadFile(@RequestBody MultipartFile file){
        fileService.importData(file);
    }


}

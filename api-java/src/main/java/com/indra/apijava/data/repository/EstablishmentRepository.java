package com.indra.apijava.data.repository;

import com.indra.apijava.data.entity.Establishment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstablishmentRepository extends CrudRepository<Establishment, Long> {
    List<Establishment> findAllByCity_Name(String cityName);
    Establishment findByCode(long code);
}

package com.indra.apijava.data.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "product")
@ToString
@NoArgsConstructor @AllArgsConstructor
public class Product implements Serializable {
    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter private long id;

    @Column(name = "product_name") @JsonProperty("product_name")
    @NonNull @Getter @Setter private String name;

    @Column(name = "product_date") @JsonProperty("collecting_date")
    @NonNull @Getter @Setter private Date collectingDate;

    @Column(name = "product_sell_value") @JsonProperty("selling_value")
    @Getter @Setter private double sellingValue;

    @Column(name = "product_buy_value") @JsonProperty("buying_value")
    @Getter @Setter private double buyingValue;

    @Column(name = "product_measure_unit") @JsonProperty("measure_unit")
    @NonNull @Getter @Setter private String measureUnit;

    @Column(name = "brand_name") @JsonProperty("brand_name")
    @NonNull @Getter @Setter private String brandName;

    @ManyToOne
    @JoinColumn(name = "establishment_code", referencedColumnName = "establishment_code")
    @JsonManagedReference
    @Getter @Setter private Establishment establishment;

}

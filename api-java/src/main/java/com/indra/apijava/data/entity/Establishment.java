package com.indra.apijava.data.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "establishment")
@ToString
@NoArgsConstructor @AllArgsConstructor
@ApiModel(description = "Entidade corresponde aos Postos de revenda")
public class Establishment implements Serializable {

    @Id
    @Column(name = "establishment_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter private long id;

    @Column(name = "establishment_code", unique = true) @JsonProperty("establishment_code")
    @Getter @Setter private long code;

    @Column(name = "establishment_name", nullable = false) @JsonProperty("establishment_name")
    @Getter @Setter private String name;

    @ManyToOne
    @JoinColumn(name = "city_name", referencedColumnName = "city_name")
    @JsonManagedReference
    @Getter @Setter private City city;

}

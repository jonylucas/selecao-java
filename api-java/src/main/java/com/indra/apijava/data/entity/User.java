package com.indra.apijava.data.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user")
@ToString
@NoArgsConstructor @RequiredArgsConstructor
@ApiModel(description = "Entidade corresponde aos usuários")
public class User {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(value = "id")
    @Getter private long id;

    @Column(name = "user_name")
    @JsonProperty(value = "name")
    @NonNull @Getter @Setter private String name;

    @Column(name = "user_login")
    @JsonProperty(value = "login")
    @NonNull @Getter @Setter private String login;

    @Column(name = "user_password")
    @JsonProperty(value = "password")
    @Getter @Setter private String password;

}

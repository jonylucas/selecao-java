package com.indra.apijava.data.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "city")
@ToString
@NoArgsConstructor @AllArgsConstructor
@ApiModel(description = "Entidade corresponde às cidades")
public class City implements Serializable {

    @Id
    @Column(name = "city_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter private long id;

    @Column(name = "city_name") @JsonProperty("city_name")
    @NonNull @Getter @Setter private String name;

    @ManyToOne
    @JoinColumn(name = "state_acronym", referencedColumnName = "state_acronym")
    @JsonManagedReference
    @Getter @Setter private State state;

}

package com.indra.apijava.data.repository;

import com.indra.apijava.data.entity.City;
import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Long> {
    City findByName(String name);
}

package com.indra.apijava.data.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "region")
@ToString
@NoArgsConstructor @AllArgsConstructor
@ApiModel(description = "Entidade corresponde às Regiões")
public class Region implements Serializable {
    @Id
    @Column(name = "region_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter private long id;

    @Column(name = "region_acronym", unique = true) @JsonProperty("region_acronym")
    @NonNull @Getter @Setter private String regionAcronym;

}

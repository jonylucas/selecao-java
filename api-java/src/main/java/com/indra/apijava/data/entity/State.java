package com.indra.apijava.data.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "state")
@ToString
@NoArgsConstructor @AllArgsConstructor
@ApiModel(description = "Entidade corresponde aos Estados")
public class State implements Serializable {

    @Id
    @Column(name = "state_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter private long id;

    @Column(name = "state_acronym", unique = true) @JsonProperty("state_acronym")
    @NonNull @Getter @Setter private String stateAcronym;

    @ManyToOne
    @JoinColumn(name = "region_acronym", referencedColumnName = "region_acronym")
    @JsonManagedReference
    @Getter @Setter private Region region;

}

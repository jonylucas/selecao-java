package com.indra.apijava.data.repository;

import com.indra.apijava.data.entity.State;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends CrudRepository<State, Long> {
    State findByStateAcronym(String acronym);
}

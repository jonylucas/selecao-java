package com.indra.apijava.data.repository;

import com.indra.apijava.data.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAllByEstablishment_Code(long code);
    Product findDistinctFirstByBrandName(String brandName);
    Product findDistinctFirstByCollectingDate(Date date);
}

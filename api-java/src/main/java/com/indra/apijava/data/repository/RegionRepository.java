package com.indra.apijava.data.repository;

import com.indra.apijava.data.entity.Region;
import com.indra.apijava.data.entity.State;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends CrudRepository<Region, Long> {
    Region findByRegionAcronym(String regionAcronym);
}

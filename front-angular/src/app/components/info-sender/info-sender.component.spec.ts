import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoSenderComponent } from './info-sender.component';

describe('InfoSenderComponent', () => {
  let component: InfoSenderComponent;
  let fixture: ComponentFixture<InfoSenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoSenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoSenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

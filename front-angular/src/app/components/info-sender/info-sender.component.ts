import { Component, OnInit } from '@angular/core';
import { GetRequestIndraService } from '../../services/get-request-indra.service'
import { InfoGetterComponent } from '../info-getter/info-getter.component'

@Component({
  selector: 'app-info-sender',
  templateUrl: './info-sender.component.html',
  styleUrls: ['./info-sender.component.css']
})
export class InfoSenderComponent implements OnInit {

  result: any[];

  constructor(private getRequest: GetRequestIndraService) { }

  ngOnInit() {
  }

  setUrlRequest(url: string){
    this.getRequest.setUrl(url);
    this.getRequest.getRequest().subscribe(result => {
      console.log(result);
      this.result = result;
    })
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { GetRequestIndraService } from '../../services/get-request-indra.service'

@Component({
  selector: 'app-info-getter',
  templateUrl: './info-getter.component.html',
  styleUrls: ['./info-getter.component.css']
})
export class InfoGetterComponent implements OnInit {

  @Input() result: any;

  constructor(private request: GetRequestIndraService) { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoGetterComponent } from './info-getter.component';

describe('InfoGetterComponent', () => {
  let component: InfoGetterComponent;
  let fixture: ComponentFixture<InfoGetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoGetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoGetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

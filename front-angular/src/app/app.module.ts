import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InfoGetterComponent } from './components/info-getter/info-getter.component';
import { InfoSenderComponent } from './components/info-sender/info-sender.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoGetterComponent,
    InfoSenderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

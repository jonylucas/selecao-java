import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetRequestIndraService {

  url: string = "http://localhost:8080/api/product";

  constructor(private http: HttpClient) { }
  
  setUrl(url: string){
    this.url = url;
    console.log(this.url);
    this.getRequest();
  }

  getRequest(): Observable<object[]>{
    return this.http.get<object[]>(this.url);
  }
}

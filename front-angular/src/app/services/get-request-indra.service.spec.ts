import { TestBed } from '@angular/core/testing';

import { GetRequestIndraService } from './get-request-indra.service';

describe('GetRequestIndraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetRequestIndraService = TestBed.get(GetRequestIndraService);
    expect(service).toBeTruthy();
  });
});
